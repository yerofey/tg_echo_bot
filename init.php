<?php

$api_token = '760630520:AAHqQhKdH7cRI-xol7ieLzQsqGg8NL9QEkU';


/**
 * @param string $method
 * @param $params
 * @return array
 */
function api(string $method, $params): array
{
    global $api_token;

    $options = is_array($params) ? http_build_query($params) : $params;
    $url = 'https://api.telegram.org/bot' . $api_token . '/' . $method . '?' . $options;
    $json = curlGet($url);
    return json_decode($json, true);
}

/**
 * @param $url
 * @param array $headers
 * @return bool|string
 */
function curlGet($url, $headers = [])
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_REFERER, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    if (!empty($headers)) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    }
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}


$webhook_url = 'https://zutix.ru/tg_bots/' . basename(__DIR__) . '/webhook.php';
print_r(api('setwebhook', 'url=' . $webhook_url));
