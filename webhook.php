<?php

$api_token = '760630520:AAHqQhKdH7cRI-xol7ieLzQsqGg8NL9QEkU';


/**
 * @param string $method
 * @param $params
 * @return array
 */
function api(string $method, $params): array
{
    global $api_token;

    $options = is_array($params) ? http_build_query($params) : $params;
    $url = 'https://api.telegram.org/bot' . $api_token . '/' . $method . '?' . $options;
    $json = curlGet($url);
    return json_decode($json, true);
}

/**
 * @param $chat_id
 * @param $action
 * @return array
 */
function botSendChatAction($chat_id, $action)
{
    return api('sendChatAction', 'chat_id=' . $chat_id . '&action=' . $action);
}

/**
 * @param $chat_id
 * @param array $options
 * @return array
 */
function botSendMessage($chat_id, $options = [])
{
    return api('sendMessage', 'chat_id=' . $chat_id . '&' . http_build_query($options));
}

/**
 * @param $url
 * @param array $headers
 * @return bool|string
 */
function curlGet($url, $headers = [])
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_REFERER, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    if (!empty($headers)) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    }
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}


// вот это магия, хз как в питоне, но тут нужно считать данные, которые были отправлены в этот же файл
$api_json = file_get_contents('php://input');
// потом переводим json в array
$updates = json_decode($api_json, true);


if (!empty($message = $updates['message'])) { // если в $updates есть подмассив message - тогда всё сохраняем в $message
    // отключаем для групп
    if ($message['chat']['type'] != 'private') {
        exit();
    }

    // работаем только если есть сообщение
    if (empty($message['text'])) {
        exit();
    }

    // бот печатает...
    botSendChatAction($message['chat']['id'], 'typing');

    // отвечаем
    botSendMessage(
        $message['chat']['id'],
        [
            'parse_mode' 	=> 'HTML',
            'text' 			=> $message['text'],
        ]
    );
}
